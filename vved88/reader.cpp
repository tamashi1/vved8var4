#include "reader.h"
#include "constants.h"

#include <fstream>
#include <cstring>


void read(const char* file_name, exchange_rates* array[], int& size)
{
    std::ifstream file(file_name);
    if (file.is_open())
    {
        size = 0;
        char tmp_buffer[MAX_STRING_SIZE];
        while (!file.eof())
        {
            exchange_rates* item = new exchange_rates;
            file >> item->bank;
            file >> item->buy;
            file >> item->sell;
            file.read(tmp_buffer, 1); // ������ ������� ������� �������
            file.getline(item->adres, MAX_STRING_SIZE);
            array[size++] = item;
        }
        file.close();
    }
    else
    {
        throw "������ �������� �����";
    }
}