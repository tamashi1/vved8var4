#include <iostream>
#include "exchange_rates.h"
#include "reader.h"
#include "filter.h"
using namespace std;
void output(exchange_rates* rates)
{
    {
        cout << "****����� �����****" << endl;
        cout << "����: " << rates->bank << '\n';
        cout << "" << rates->sell << " : ";
        cout << rates->buy << '\n';
        cout << rates->adres << '\n';
        cout << '\n';
    }
}

int main()
{
    setlocale(LC_ALL, "Russian");
    cout << "������������ ������ �8. GIT\n";
    cout << "������� �4. ����� �����\n";
    cout << "�����: ����� ����\n";
    cout << "������: 11\n";
    cout << "***********" << endl;
    cout << "Laboratory work #8. GIT\n";
    cout << "Variant #4. Bank Subscription\n";
    cout << "Author: Karim Issa\n";
    cout << "Group: 11\n";

	exchange_rates* rates[MAX_FILE_ROWS_COUNT];
	int size;
	try
	{
		read("�����.txt", rates, size);
		cout << "*****   *****\n\n";
		for (int i = 0; i < size; i++)
		{
			output(rates[i]);
		}
		bool (*check_function)(exchange_rates*) = NULL;

		int item;
		cin >> item;
		cout << '\n';
		switch (item)
		{
		case 1:
			check_function = check_by_bank;       
			cout << "*****    �����������    *****\n\n";
			break;
		case 2:
			check_function = check_by_sell;       
			cout << "*****    ������� ������ 2.5  *****\n\n";
			break;
		
		default:
			throw "  ";
		}
		if (check_function)
		{
			int new_size;
			exchange_rates** filtered = filter(rates, size, check_function, new_size);
			for (int i = 0; i < new_size; i++)
			{
				output(filtered[i]);
			}
			delete[] filtered;
		}
		for (int i = 0; i < size; i++)
		{
			delete rates[i];
		}
	}
	catch (const char* error)
	{
		cout << error << '\n';
	}
	return 0;
}
