#ifndef EXCHANGE_RATES_H
#define EXCHANGE_RATES_H

#include "constants.h"


struct exchange_rates
{
    char bank[MAX_STRING_SIZE];
    double buy;
    double sell;
    char adres[MAX_STRING_SIZE];
};

#endif